angular.module('xplora', ['ionic', 'xplora.controllers', 'xplora.services'])
.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})
.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider
    .state('app', {
      url: '/app',
      abstract: true,
      templateUrl: 'templates/menu.html',
      controller: 'MainCtrl'
    })
    .state('app.hostels', {
      url: '/hostels',
      views: {
        'app-hostels': {
          templateUrl: 'templates/hostels.html',
          controller: 'HostelsController'
        }
      }
    })
    .state('app.search', {
      url: '/search',
      views: {
        'app-hostels': {
          templateUrl: 'templates/search.html',
          controller: 'SearchCtrl'
        }
      }
    })
    .state('app.hostel', {
      url: '/hostels/:hostelId',
      views: {
        'app-hostels': {
          templateUrl: 'templates/hostel.html',
          controller: 'HostelController'
        }
      }
    })
    .state('app.hostel-map', {
      url: '/hostels/map/:hostelId',
      views: {
        'app-hostels': {
          templateUrl: 'templates/map.html',
          controller: 'MapCtrl'
        }
      }
    })
    .state('app.about', {
      url: '/about',
      views: {
        'app-hostels': {
          templateUrl: 'templates/about.html'
        }
      }
    });

  $urlRouterProvider.otherwise('/app/hostels');
});
