angular.module('xplora.services', [])
  .service('HostelProvider', [function() {
    var hostelsData = window.HOSTELS.map(function(hostel, i) {
      hostel.id = i;
      return hostel;
    }).sort(function (a, b) {
      if (a.displayName < b.displayName) return -1;
      return 1;
    });

    return {
      getAll: function() {
        return hostelsData;
      },
      getById: function(id) {
        var hostelFound = hostelsData.filter(function(hostel) {
          return hostel.id.toString() === id;
        });

        if (hostelFound.length) return hostelFound[0];

        return null;
      },
      getByLocation: function(location) {
        var hostelsFound = hostelsData.filter(function(hostel) {
          return hostel.location === location;
        });

        return hostelsFound;
      }
    };
  }]);

window.HOSTELS = [{
    displayName: 'Steven Paris Hostel',
    name: 'Steven Paris Hostel',
    location: 'Campus',
    locationSearchName: 'Steven Paris Hostel',
    imageSrc: 'icon.png',
    prices: {
      previous: null,
      current: {
        one: 2520,
        two: 1260
      }
    },
    comments: 'Room facilities are quite good. Rooms are spacious depending on the number of persons occupying it Dimension of self contained is about 14 x 12. Also there are 2 flats in the hostel which have AC.',
    information: 'Mainly provides accomodation for foreign students who offer Water Supply and Sanitation at post-graduate level. Accomodation is obtained only through application. Vacancy is opened to Ghanaianss when there are still vacancy after the foreigners have finished applying.',
    facilities: ['DSTV', 'Water Supply', 'Electricity (No plant)'],
    contacts: []
  }, {
    displayName: 'Brunei Hostel',
    name: 'Otumfuo Opoku Ware II, Nana Agyemang Prempeh II & NANA AFIA SERWAA KOBI AMPEM',
    location: 'Campus',
    locationSearchName: 'Brunei Hostel',
    imageSrc: 'brunei.jpg',
    prices: {
      previous: {
        one: 1650,
        two: 1400
      },
      current: {
        one: 1850,
        two: 1600
      }
    },
    comments: 'Quite good and rooms are spacious depending on the number of persons.',
    information: 'Accomodation is aquired through application form.',
    facilities: ['DSTV', 'Water Supply', 'Electricity (No plant)'],
    contacts: []
  }, {
    displayName: 'Tech Credit',
    name: 'Tech Credit',
    location: 'Campus',
    locationSearchName: 'Tech Credit',
    imageSrc: 'icon.png',
    prices: {
      previous: {
        two: 1750,
        three: 1350
      },
      current: {
        two: 2300,
        three: 1550
      }
    },
    comments: 'Quite good and rooms are spacious depending on the number of persons.',
    information: 'Accomodation is aquired through application form which costs 2 Ghana Cedis',
    facilities: ['DSTV', 'Water Supply', 'Electricity (Plant)'],
    contacts: ['0322060228']
  },
  // TODO: remeber to add prices for local students
  {
    displayName: 'Graduate Students Hostel',
    name: 'Graduate Students Hostel',
    location: 'Campus',
    locationSearchName: 'Graduate Students Hostel',
    imageSrc: 'icon.png',
    prices: {
      previous: null,
      current: {
        two: 3000
      }
    },
    comments: 'Quite good and rooms are spacious (14 x 14) approximately. Flat has 3 rooms, 2 toilets and 1 kitchen.',
    information: 'Accomodation for only post graduate students and this is done through application form.',
    facilities: ['DSTV', 'Water Supply', 'Electricity (No plant)', 'Security'],
    contacts: []
  }, {
    displayName: 'Chancellors Hall / Hall 7',
    name: 'Chancellors Hall / Hall 7',
    location: 'Campus',
    locationSearchName: 'Hall 7',
    imageSrc: 'icon.png',
    prices: {
      previous: null,
      current: {
        two: 1800,
        three: 1400,
        four: 1100
      }
    },
    comments: 'Quite good and rooms are spacious depending on the number of persons.',
    information: 'Accomodation is aquired through application form. This is done in the months of February and March and deadline is between May and June.',
    facilities: ['DSTV', 'Water Supply', 'Electricity (Plant)', 'Security', 'Cleaning Services'],
    contacts: []
  }, {
    displayName: 'Nyame-Mireku Plaza',
    name: 'Nyame-Mireku Plaza',
    location: 'Kotei',
    locationSearchName: 'Nyame-Mireku Plaza',
    imageSrc: 'icon.png',
    prices: {
      previous: {
        three: 1350
      },
      current: {
        three: 1650
      }
    },
    comments: 'Well furnished rooms with tiled floors. Good ventilation system and spacious. Water pumping machine is also available',
    information: 'The hostel has six flats with three bedrooms in each flat. A common hall and kitchen(well-furnished). It\'s a 3-storey hostel facility with stores occupying the ground floor. Payment is inclusive of water supply. Electricity (Prepaid) and gas are paid by renters but are provided with some at the beginning of every semester.',
    facilities: ['DSTV', 'Water Supply', 'Electricity (Plant)', 'Generator'],
    contacts: ['0245396040']
  },
  {
    displayName: 'Shepherdville',
    name: 'Shepherdville',
    location: 'Kotei',
    locationSearchName: 'Shepherdville',
    imageSrc: 'shepherdsville.jpg',
    prices: {
      previous: {
        one: 1600,
        two: 1300,
        three: 900,
        four: 600
      },
      current: {
        one: 2200,
        two: 1800,
        three: 1200,
        four: 900
      }
    },
    comments: 'Good ventilation system. Rooms are spacious with tiled floors. Rooms are also furnished with tables and chairs. Has a common kitchen on each floor.',
    information: 'Shepherdville is a well built hostel with an active standby plant (to generate electricity). It is a 4-storey building.',
    facilities: ['DSTV', 'Water Supply', 'Electricity (Plant)'],
    contacts: ['0268595070']
  },
  {
    displayName: 'Nyberg Hostel',
    name: 'Nyberg Hostel',
    location: 'Kotei',
    locationSearchName: 'Nyberg Hostel',
    imageSrc: 'nyberg.jpg',
    prices: {
      previous: {
        two: 1250,
        three: 900
      },
      current: {
        two: 1500,
        three: 1300
      }
    },
    comments: 'Spacious rooms with wardrobe and tiled floors. Has a common kitchen.',
    information: 'A 2-storey building hostel facility',
    facilities: ['DSTV', 'Water Supply', 'Electricity (Plant)', 'Generator', 'Gym'],
    contacts: ['0205883354']
  },
  {
    displayName: 'Delisa Meah',
    name: 'Delisa Meah',
    location: 'Kotei',
    locationSearchName: 'Delisa Hostel',
    imageSrc: 'olddelisa.jpg',
    prices: {
      previous: {
        two: 1200,
        three: 900
      },
      current: {
        two: 1600,
        three: 1300
      }
    },
    comments: 'Rooms are spacious with tiled floors and balcony and a common kitchen',
    information: 'The hostel facility has a general tv room and has 3 flats',
    facilities: ['DSTV', 'Water Supply', 'Electricity (Plant)', 'Generator (Not active enough)'],
    contacts: ['0509123162']
  },
  {
    displayName: 'Eunivic Hostel',
    name: 'Eunivic Hostel',
    location: 'Kotei',
    locationSearchName: 'Eunivik Hostel',
    imageSrc: 'eunivic.jpg',
    prices: {
      previous: {
        two: 1300
      },
      current: {
        two: 1500
      }
    },
    comments: 'Spacious rooms with tiled floors and common kitchen',
    information: 'A 3-storey hostel facility',
    facilities: ['Water Supply', 'Electricity (Plant)', 'Study room', 'TV room'],
    contacts: ['0240702726']
  },
  {
    displayName: 'Jital Hostel',
    name: 'Jital Hostel',
    location: 'Kotei',
    locationSearchName: 'Jital Hostel',
    imageSrc: 'jital.jpg',
    prices: {
      previous: {
        two: 900,
        three: 750
      },
      current: {
        two: 1100,
        three: 900
      }
    },
    comments: 'Rooms are spacious. They have tv room and study rooms.',
    information: 'It is a 2-storey hostel facility',
    facilities: ['Water Supply', 'Electricity (Plant)', 'Generator'],
    contacts: ['0249338350']
  },
  {
    displayName: 'Long Island',
    name: 'Long Island',
    location: 'Kotei',
    locationSearchName: 'Long Island',
    imageSrc: 'island.jpg',
    prices: {
      previous: {
        two: 1700,
        three: 1100
      },
      current: {
        two: 1900,
        three: 1300
      }
    },
    comments: 'Well furnished rooms, tv room and study rooms.',
    information: '',
    facilities: ['DSTV', 'Water Supply', 'Electricity (Plant)'],
    contacts: ['0543247175']
  },
  {
    displayName: 'Hydes Hostel',
    name: 'Hydes Hostel',
    location: 'Kotei',
    locationSearchName: 'Hydes Hostel',
    imageSrc: 'hydes.jpg',
    prices: {
      previous: {
        two: 1100
      },
      current: {
        two: 1600,
        four: 900
      }
    },
    comments: 'Spacious rooms, a common kitchen for each floor.',
    information: '3-storey hostel complex with spacious compound neatly kept, security post.',
    facilities: ['DSTV', 'Water Supply', 'Electricity (Plant)'],
    contacts: ['0244372453', '0322063846']
  },
  {
    displayName: 'Delisa Annex',
    name: 'Delisa Annex',
    location: 'New Site',
    locationSearchName: 'Delisa Annex',
    imageSrc: 'icon.png',
    prices: {
      previous: {
        one: 2000,
        two: 1200,
        three: 900
      },
      current: {
        one: 2800,
        two: 1600,
        three: 1300,
      }
    },
    comments: 'Rooms are spacious. Floors are tiled',
    information: '79 rooms with 4 general kitchens. 3 offices, 1 store room and 1 TV room. The road networks are in good state',
    facilities: ['DSTV', 'Water Supply', 'Electricity (Plant)', 'Cleaning Services every Saturday morning'],
    contacts: ['0268979890']
  },
  {
    displayName: 'Fosua Homes',
    name: 'Fosua Homes',
    location: 'New Site',
    locationSearchName: 'Fosua Homes',
    imageSrc: 'icon.png',
    prices: {
      previous: {
        one: 2000,
        two: 1000,
        three: 800
      },
      current: {
        one: 2400,
        two: 1200,
        three: 900
      }
    },
    comments: 'The rooms are quite spacious',
    information: 'Study rooms and tv roooms.',
    facilities: ['DSTV', 'Water Supply', 'Electricty (Plant)'],
    contacts: ['0502233674']
  },
  {
    displayName: 'Glory be to God (Annex)',
    name: 'Glory be to God (Annex)',
    location: 'New Site',
    locationSearchName: 'Glory be to God',
    imageSrc: 'icon.png',
    prices: {
      previous: {
        two: 1250
      },
      current: {
        one: 3000,
        two: 2000
      }
    },
    comments: 'Tiles. Furnishing already given (TV stand). Painted.',
    information: 'There\'s a kitchen in the room. The general outlook of the room is very very spacious. The electricity system is a post paid method but will soon be changed to prepaid.',
    facilities: ['DSTV', 'Water Supply', 'Electricity (Plant)', 'Cleaning Services'],
    contacts: ['0249306097']
  },
  {
    displayName: 'Splendor (Old Block)',
    name: 'Splendor (Old Block)',
    location: 'New Site',
    locationSearchName: 'Splendor',
    imageSrc: 'icon.png',
    prices: {
      previous: {
        one: 2400,
        two: 1200,
        three: 900,
        four: 800
      },
      current: {
        one: 3600,
        two: 1800,
        three: 1200,
        one: 1000
      }
    },
    comments: 'Well painted. There are no AC. Nothing extradinary. Bathrrom Tiled',
    information: 'The road leading to the property is in the adeplorable state.',
    facilities: ['DSTV', 'Water Supply', 'Electricity (Plant)'],
    contacts: ['0244251558']
  },
  {
    displayName: 'Caesar\'s Palace',
    name: 'Caesar\'s Palace',
    location: 'New Site',
    locationSearchName: 'Caesar\'s Palace',
    imageSrc: 'icon.png',
    prices: {
      previous: {
        one: 1700,
        two: 800,
        three: 700
      },
      current: {
        one: 1900,
        two: 1000,
        three: 900
      }
    },
    comments: 'Rooms are good but small in size.',
    information: 'The water supply is not consistent. Those on the ground floor have constant water supply but not upstairs. No study room. Building in progress since last year. Security is average.',
    facilities: ['DSTV', 'Electricity (Plant)'],
    contacts: ['0545436004']
  },
  {
    displayName: 'Providence Hostel',
    name: 'Providence Hostel',
    location: 'New Site',
    locationSearchName: 'Providence Hostel',
    imageSrc: 'providence.jpg',
    prices: {
      previous: {
        two: 1400
      },
      current: {
        three: 1140,
        four: 1000
      }
    },
    comments: 'Well furnished spacious rooms, tv room, common kitchen for each floor. Most of the rooms are 4 in a room.',
    information: '2-storey hostel facility with a cafetaria occupying the basement. A basket ball court, spacious grounds for parties, weddings, etc. Secretarial servicess, laundry services.',
    facilities: ['DSTV', 'Water Supply', 'Electricity (Plant)', 'Standby generator'],
    contacts: ['0208464917']
  },
  {
    displayName: 'Wilkado',
    name: 'Wilkado',
    location: 'Gaza',
    locationSearchName: 'Wilkado Hostel',
    imageSrc: 'wilkado.jpg',
    prices: {
      previous: {
        one: 2800,
        two: 1500,
        three: 1100
      },
      current: {
        one: 3000,
        two: 1800,
        three: 1400
      }
    },
    comments: 'Quite large rooms and wardropes',
    information: 'Wash rooms and kitchens within room space. Study rooms and prepaid.',
    facilities: ['DSTV', 'Water Supply', 'Electricity', 'No Plant'],
    contacts: ['0302970636', '0302813704']
  },
  {
    displayName: 'Suncity',
    name: 'Suncity',
    location: 'Gaza',
    locationSearchName: 'Suncity Hostel',
    imageSrc: 'suncity.jpg',
    prices: {
      previous: {
        one: 3200,
        two: 1600,
        three: 1100
      },
      current: {
        one: 4000,
        two: 2000,
        three: 1400
      }
    },
    comments: 'Quite large rooms and wardropes available',
    information: 'Cafetaria. Washroom are within room space. Close proximity to Georgia Hostel. Prepaid Electricity.',
    facilities: ['DSTV', 'Water Supply', 'Electricity', 'No Plant'],
    contacts: ['0548471371', '0240201834']
  },
  {
    displayName: 'Georgia',
    name: 'Georgia',
    location: 'Gaza',
    locationSearchName: 'Georgia Hostel',
    imageSrc: 'georgia.jpg',
    prices: {
      previous: {
        one: 2800,
        two: 1500
      },
      current: {
        one: 3000,
        two: 1800
      }
    },
    comments: 'Rooms are quite large, wardrobe available, study room',
    information: ['Security guaranteed.', 'Errand boys around', 'Ebola sanitization provided', 'Study rooms on every floor', 'Washrooms are within room space', 'Close proximity to Gaza and Crystal Rose'].join('\n'),
    facilities: ['DSTV', 'Water Supply', 'Electricity', 'No Plant', 'Shopping mall'],
    contacts: ['0243986830']
  },
  {
    displayName: 'Gaza',
    name: 'Gaza',
    location: 'Gaza',
    locationSearchName: 'Gaza Hostel',
    imageSrc: 'gaza.jpg',
    prices: {
      previous: {
        one: 3000,
        two: 1800
      },
      current: {
        one: 3500,
        two: 2000
      }
    },
    comments: 'Spacious rooms and wardrobes.',
    information: ['Close proximity to shops', 'All rooms have kitchen and washrooms within the room space', 'Shuttle services available', 'A spacios courtyard', 'Reading rooms on every floor', 'Prepaid electricity by tenants'].join('\n'),
    facilities: ['DSTV', 'Water Supply', 'Electricity (Plant)'],
    contacts: ['0208181939']
  },
  /*{
    displayName: 'Suncity',
    name: 'Suncity',
    location: 'Gaza',
    locationSearchName: 'Suncity',
    imageSrc: 'suncity.jpg',
    prices: {
      previous: {
        one: 2800,
        two: 1500
      },
      current: {
        one: 3000,
        two: 2000
      }
    },
    comments: 'Quite large rooms and wardropes',
    information: ['washrooms and kitchen is within the room space', 'Close proximity ti shops', 'General reading room', 'Cafetaria', 'Electricity and water bills catered for'].join('\n'),
    facilities: ['DSTV', 'Water Supply', 'Electricity (Plant)', 'Laundry Services'],
    contacts: ['0246666386']
  },*/
  {
    displayName: 'Anglican',
    name: 'Anglican',
    location: 'Gaza',
    locationSearchName: 'Anglican Hostel, Gaza, Kumasi',
    imageSrc: 'icon.png',
    prices: {
      previous: {
        one: 1800,
        two: 800,
        three: 700
      },
      current: {
        one: 2000,
        two: 900,
        three: 800
      }
    },
    comments: 'Wardrobe, basic single bed, kitchen in flats (1 in room)',
    information: ['Shuttle services available', 'Common rooms (2 and 3 in a room)', 'Share common washrooms', 'Flats (1 in a room) have kitchen and washroom facilities within.', 'Study room/ Conference room', 'Prepaidmeters paid by students'].join('\n'),
    facilities: ['DSTV', 'Water Supply', 'Electricity (No Plant)', 'Football field', 'Supermarket'],
    contacts: ['0208521865']
  },
  {
    displayName: 'Nana Adoma Hostel',
    name: 'Nana Adoma Hostel',
    location: 'Ayeduase',
    locationSearchName: 'Nana Adoma Hostel',
    imageSrc: 'icon.png',
    prices: {
      previous: {
        two: 800,
        four: 650
      },
      current: {
        two: 1100,
        four: 800
      }
    },
    comments: 'Big-sized beds',
    information: ['Slow response rate to complaints', 'Ground floors are floodable', 'Construction is fairly old', 'Water Supply is inconsistent', 'Recurrent electric faults'].join('\n'),
    facilities: ['Water Supply'],
    contacts: []
  },
  {
    displayName: 'Adom Bi Hostel',
    name: 'Adom Bi Hostel',
    location: 'Ayeduase',
    locationSearchName: 'Adom Bi Hostel',
    imageSrc: 'icon.png',
    prices: {
      previous: {
        two: 1400,
        three: 900,
        four: 750
      },
      current: {
        two: 1800,
        three: 1300,
        four: 1000
      }
    },
    comments: 'Sizeable rooms, well furnished floors, walls and ceilings. Average response rate to maintenance issues.',
    information: 'No available information',
    facilities: ['DSTV', 'Water Supply', 'Electricity (Plant)'],
    contacts: ['0275675787']
  }
];
