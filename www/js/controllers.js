angular.module('xplora.controllers', [])
  .controller('MainCtrl', ['$scope', '$state', function($scope, $state) {
    $scope.navigate = function(stateToNavigateTo, $event) {
      $event.stopPropagation();
      $state.go('app.' + stateToNavigateTo);
    };
  }])
  .controller('HostelsController', ['$scope', 'HostelProvider', '$state', function($scope, HostelProvider, $state) {
    $scope.hostels = HostelProvider.getAll();
    $scope.goToHostel = function(id, $event) {
      $event.stopPropagation();
      $state.go('app.hostel', {
        hostelId: id
      });
    };
  }])
  .controller('SearchCtrl', ['$scope', 'HostelProvider', '$state', '$ionicPopover', function($scope, HostelProvider, $state, $ionicPopover) {
    var localStorageExists = !!window.localStorage;

    function getHostelsForScope() {
      $scope.hostels = $scope.currentLocation.value === 'None' ? HostelProvider.getAll() : HostelProvider.getByLocation($scope.currentLocation.value);
    }

    $ionicPopover.fromTemplateUrl('templates/location-popover.html', {
      scope: $scope,
    }).then(function(popover) {
      $scope.popover = popover;
    });

    $scope.locations = [{
      name: 'Campus',
      value: 'cp'
    }, {
      name: 'Ayeduase',
      value: 'ay'
    }, {
      name: 'Kotei',
      value: 'kt'
    }, {
      name: 'New Site',
      value: 'ns'
    }, {
      name: 'Bomso',
      value: 'bm'
    }, {
      name: 'Gaza',
      value: 'gz'
    }, {
      name: 'None',
      value: ''
    }];

    var search;
    if (localStorageExists) {
      if (localStorage.search && (search = JSON.parse(localStorage.search))) {
        $scope.query = search.query;
        $scope.currentLocation = search.currentLocation;
      }
    }

    if (!search) {
      $scope.query = {
        displayName: ''
      };
      $scope.currentLocation = {
        value: 'None'
      };
    }

    getHostelsForScope();

    $scope.$on('$destroy', function() {
      $scope.popover.remove();
    });
    $scope.openPopover = function($event) {
      $scope.popover.show($event);
    };
    $scope.goToHostel = function(id, $event) {
      $event.stopPropagation();
      $state.go('app.hostel', {
        hostelId: id
      });
    };
    $scope.locationChanged = function() {
      getHostelsForScope();
      localStorageExists && (localStorage.search = JSON.stringify({
        query: $scope.query,
        currentLocation: $scope.currentLocation
      }));
    };
    $scope.queryChanged = function(query) {
      $scope.query = query;
      localStorageExists && (localStorage.search = JSON.stringify({
        query: query,
        currentLocation: $scope.currentLocation
      }));
    };
  }])
  .controller('HostelController', ['$scope', 'HostelProvider', '$stateParams', '$state', function($scope, HostelProvider, $stateParams, $state) {
    $scope.hostel = HostelProvider.getById($stateParams.hostelId);
    $scope.nearByHostels = [];
    HostelProvider.getByLocation($scope.hostel.location).forEach(function(_hostel) {
      if (_hostel.id !== $scope.hostel.id) {
        $scope.nearByHostels.push(_hostel);
      }
    });
    $scope.goToMap = function($event) {
      $event.stopPropagation();
      $state.go('app.hostel-map', {
        hostelId: $scope.hostel.id
      });
    };
  }])
  .controller('MapCtrl', ['$scope', '$ionicLoading', '$timeout', 'HostelProvider', '$stateParams', '$state', '$ionicHistory', '$ionicPosition', '$ionicPopup', function ($scope, $ionicLoading, $timeout, HostelProvider, $stateParams, $state, $ionicHistory, $ionicPosition, $ionicPopup) {
    var mapContainer = document.getElementById('map-container'),
        place,
        numAlerted = 0;

    $scope.msg = '';
    $scope.showMap = true;
    $scope.showMsg = false;
    $scope.hostel = HostelProvider.getById($stateParams.hostelId);
    place = $scope.hostel.locationSearchName + ' , KNUST Ghana';

    if (window.cordova && navigator.connection.type === Connection.NONE) {
      $scope.showMap = false;

      $ionicPopup.alert({template:'Please make sure you are connected to the internet', title: 'Hostel Xplora'});
      $timeout(function () {
        $ionicHistory.goBack();
      }, 3000);
      return;
    }

    $scope.$on('$ionicView.afterEnter', function() {
      mapContainer.style.height = '' + (window.innerHeight - $ionicPosition.offset(document.getElementsByClassName('title-header')).height) + 'px';
      if (window.ISLOADED_MAPS) {
        searchLocation();
      } else {
        $ionicLoading.show({
          templateUrl: 'templates/load.html',
          scope: $scope,
          hideOnStateChange: true,
          noBackDrop: true
        });

        $timeout(function () {
          $ionicLoading.hide();
          if (window.ISLOADED_MAPS) {
            searchLocation();
            return;
          }
          $ionicPopup.alert({template: 'Please make sure you are connected to the internet and try again', title: 'Hostel Xplora'});
          $scope.showMap = false;
          $timeout(function () {
            var initialize = window.initialize;
            window.loadScript();
          }, 10000);
        }, 5000)
      }
      function searchLocation() {
        if (!window.NOTIFIED) {
          $ionicPopup.alert({template: 'Please turn on GPS if it\'s off', title: 'Xplora GPS'});
          window.NOTIFIED = true;
        }
        navigator.geolocation.getCurrentPosition(performSearch);
        navigator.geolocation.watchPosition(performSearch);
      }

      function performSearch(pos) {
        var directionsDisplay = new google.maps.DirectionsRenderer(),
            pyrmont = new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude);
        map = new google.maps.Map(mapContainer, {
          center: pyrmont,
          zoom: 13,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        directionsDisplay.setMap(map);

        var request = {
          location: pyrmont,
          radius: 5000,
          query: place
        };

        service = new google.maps.places.PlacesService(map);
        service.textSearch(request, callback);

        function callback(results, status) {
          if (status == google.maps.places.PlacesServiceStatus.OK) {
            var request = {
              origin: new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude),
              destination: results[0].geometry.location,
              travelMode: google.maps.TravelMode.WALKING
            };

            window.directionsService.route(request, function(result, status) {
              if (status == google.maps.DirectionsStatus.OK) {
                directionsDisplay.setDirections(result);
              }
            });
          } else {
            if (numAlerted > 0) return;
            $ionicPopup.alert({template: 'Google Maps does not have data for ' + $scope.hostel.displayName, title: 'Hostel Xplora'});
            numAlerted = 1;
          }
        }
      }
    });
  }])
